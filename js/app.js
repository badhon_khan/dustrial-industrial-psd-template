// IIFE with jQuery Wrapper
(function($) {
  'use strict';

  /*
   *----------------------------------
   * Document Ready
   *----------------------------------
   */
	$(document).ready(function() {

    /****************************
      stellarnav DropDown Menu
    *****************************/
    $('.stellarnav').stellarNav();

    /****************************
      Owl Carousel Plugin
    *****************************/
    $('#main-slider').owlCarousel({
      loop:true,
      margin:0,
      nav:false,
      dots: true,
      items:1
    })

    /****************************
      Testimonials Items Slider
    *****************************/
    $('#testimonial-items').owlCarousel({
      loop:true,
      margin:30,
      nav:true,
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      dots: false,
      items:2,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          }
      }
    })

    $('#testimonial-items-2').owlCarousel({
      loop:true,
      margin:30,
      nav:false,
      dots: true,
      items:2,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          }
      }
    })

    $('#testimonial-items-3').owlCarousel({
      loop:true,
      margin:30,
      nav:true,
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      dots: false,
      items:2,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          }
      }
    })

    /****************************
      Counter Up
    *****************************/
    if ( $('.work-counter').length != 0 ) {
      var a = 0;
      $(window).scroll(function() {
        var oTop = $('.work-counter').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
          $( document ).ready(function(){
            
            $('.counter').each(function() {
              var $this = $(this),
                  countTo = $this.attr('data-count');
              
              $({ countNum: $this.text()}).animate({
                countNum: countTo
              },{
                duration: 8000,
                easing:'linear',
                step: function() {
                  $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                  $this.text(this.countNum);
                  //alert('finished');
                }
              })  
            });

          });
          a = 1;
        }
      });
    }

    /****************************
      MixItUp Activation
    *****************************/
    if ( $('#mixitup-projects').length != 0 ) {
      var containerEl = document.querySelector('#mixitup-projects');
      var mixer = mixitup(containerEl, {
          selectors: {
              control: '[data-mixitup-control]'
          }
      })
    }

    /****************************
      Testimonials Items Slider
    *****************************/
    $('#blog-list').owlCarousel({
      loop:true,
      margin:30,
      nav:true,
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      dots: false,
      smartSpeed: 1000,
      items:2,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          },
          992:{
              items:3
          }
      }
    })

    $('#blog-list-2').owlCarousel({
      loop:true,
      margin:30,
      nav:true,
      navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
      dots: false,
      smartSpeed: 1000,
      items:2,
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          },
          992:{
              items:2
          }
      }
    })

    /****************************
      Magnific Popup
    *****************************/
    // Inline popups
    $('#inline-popups').magnificPopup({
      delegate: 'a',
      removalDelay: 500, //delay removal by X to allow out-animation
      callbacks: {
        beforeOpen: function() {
           this.st.mainClass = this.st.el.attr('data-effect');
        }
      },
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

    // Inline popups
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false
    });

    // Line Bar Progressbar
    $('#bar1').barfiller();
    $('#bar2').barfiller();
    $('#bar3').barfiller();
    $('#bar4').barfiller();

    // wow js active
    new WOW().init();

    //
  // Google map

  function basicmap() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 11,
            scrollwheel: false,
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(40.6700, -73.9400), // New York
            // This is where you would paste any style found on Snazzy Maps.
            styles: [
              {
                  "featureType": "administrative.country",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      },
                      {
                          "hue": "#ffdda7"
                      }
                  ]
              }
          ]
        };
        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('contact-map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var customMarker = '../img/map-maker.png';
        var marker = new google.maps.Marker({
            icon: customMarker, 
            position: new google.maps.LatLng(40.692626, -73.9256118),
            map: map,
            title: 'Dustrial'
        });
    }
    if ($('#contact-map').length != 0) {
        google.maps.event.addDomListener(window, 'load', basicmap);
    }


	});// DOM Ready


}(jQuery)); // IIFE

